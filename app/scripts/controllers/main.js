'use strict';

/**
 * @ngdoc function
 * @name projectTemplateApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the projectTemplateApp
 */
angular.module('projectTemplateApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
