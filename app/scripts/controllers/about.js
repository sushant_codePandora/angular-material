'use strict';

/**
 * @ngdoc function
 * @name projectTemplateApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the projectTemplateApp
 */
angular.module('projectTemplateApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
